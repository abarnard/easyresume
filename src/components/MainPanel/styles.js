/*
 * Author: Anje Barnard
 */

import Color from 'Color.js'
import Size from 'Size.js'

const styles = {
  mainPanel: {
    backgroundColor: Color.bgMain,
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    padding: Size.pannelPadding,
    paddingTop: Size.pannelPadding + Size.headingMargin
  },
  section: {
    marginBottom: Size.headingMargin
  }
};

export default styles