/*
 * Author: Anje Barnard
 */

import EducationIcon from 'material-ui-icons/School'
import ExperienceEntry from 'components/ExperienceEntry'
import OtherEntry from 'components/OtherEntry'
import React from 'react'
import resumeData from 'resumeData'
import SectionHeader from 'components/SectionHeader'
import styles from './styles'
import WorkIcon from 'material-ui-icons/Work'

class MainPanel extends React.Component {
  render() {
    return (
      <div style={styles.mainPanel}>
        <div style={styles.section}>
          <SectionHeader headerIcon={WorkIcon} headerText='Experience'/>
          {
            resumeData.experience.map((experience) => (
              <ExperienceEntry  key={experience.title}
                                leftText={experience.title}
                                rightText={experience.date}
                                bullets={experience.bullets}/>
            ))
          }
        </div>
        <div style={styles.section}>
          <SectionHeader headerIcon={EducationIcon} headerText='Education'/>
          {
            resumeData.education.map((education) => (
              <ExperienceEntry  key={education.title}
                                leftText={education.title}
                                rightText={education.date}
                                bullets={education.bullets}/>
            ))
          }
        </div>
        {
          resumeData.other.map((other) => {
            // TODO: perf improvement: preload the icons in componentWillMount?
            const sectionIcon = require(`material-ui-icons/${other.sectionIcon}`).default;
            return (
              <div key={other.sectionName} style={styles.section}>
                <SectionHeader headerIcon={sectionIcon} headerText={other.sectionName}/>
                {
                  other.entries.map((entry, i) => (
                    <OtherEntry   key={`other-entry-${i}`}
                                  title={entry.title}
                                  entryItems={entry.items}/>
                  ))
                }
              </div>
            )
          })
        }
      </div>
    );
  }
}

export default MainPanel