/*
 * Author: Anje Barnard
 */

import React from 'react'
import styles from './styles'
import Typography from 'material-ui/Typography'

class SectionHeader extends React.Component {
  render() {
    return (
      <div style={styles.sectionHeader}>
        <div style={styles.sectionHeaderIconContainer}>
          <this.props.headerIcon style={styles.sectionHeaderIcon}/>
        </div>
        <Typography type='title' style={styles.sectionHeaderText}>{this.props.headerText}</Typography>
      </div>
    )
  }
}

export default SectionHeader