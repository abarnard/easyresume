/*
 * Author: Anje Barnard
 */

import Color from 'Color.js'
import Size from 'Size'

const styles = {
  sectionHeader: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: Size.headingMargin
  },
  sectionHeaderIconContainer: {
    width: 40,
    height: 40,
    borderRadius: '50%',
    backgroundColor: Color.bgAccent
  },
  sectionHeaderIcon: {
    color: Color.textMainPrimary,
    width: 32,
    height: 32,
    margin: 4
  },
  sectionHeaderText: {
    fontWeight: 400,
    marginLeft: 12,
    textTransform: 'uppercase'
  }
};

export default styles