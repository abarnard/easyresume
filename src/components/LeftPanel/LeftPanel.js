/*
 * Author: Anje Barnard
 */

import Avatar from 'material-ui/Avatar'
import EmailIcon from 'material-ui-icons/Email'
import HomeIcon from 'material-ui-icons/Home'
import PhoneIcon from 'material-ui-icons/Phone'
import React from 'react'
import resumeData from 'resumeData'
import styles from './styles'
import Typography from 'material-ui/Typography'
import WebIcon from 'material-ui-icons/Web'

class LeftPanel extends React.Component {
  render() {
    return (
      <div style={styles.leftPanel}>
        <div style={styles.imageContainer}>
          <Avatar
            alt={resumeData.name}
            src="/images/ResumePic.jpg"
            style={styles.image}
          />
        </div>
        <Typography style={styles.name} type='headline'>{resumeData.name}</Typography>
        <Typography style={styles.careerTitle} type='headline' gutterBottom>{resumeData.careerTitle}</Typography>
        <Typography style={styles.aboutMe} type='body1' gutterBottom>{resumeData.intro}</Typography>
        <div style={styles.contactItems}>
          <div style={styles.contactContainer}>
            <PhoneIcon style={styles.contactIcon}/>
            <a style={styles.contactText} href='tel:+1 902 402 0709'>{resumeData.phone}</a>
          </div>
          <div style={styles.contactContainer}>
            <EmailIcon style={styles.contactIcon}/>
            <a style={styles.contactText} href='mailto:anjebarnard@gmail.com' target='_top'>
              {resumeData.email}
            </a>
          </div>
          {resumeData.web &&  <div style={styles.contactContainer}>
                                <WebIcon style={styles.contactIcon}/>
                                <a style={styles.contactText} href={resumeData.web} target='_system'>{resumeData.web}</a>
                              </div>
          }
          <div style={styles.contactContainer}>
            <HomeIcon style={styles.contactIcon}/>
            <div style={styles.contactText}>{resumeData.locationHome}</div>
          </div>
        </div>
      </div>
    )
  }
}

export default LeftPanel