/*
 * Author: Anje Barnard
 */

import Color from 'Color.js'
import Size from 'Size.js'

const imgSize = Size.imgLarge;
const borderSize = 4;

const styles = {
  leftPanel: {
    backgroundColor: Color.bgIntroContact,
    maxWidth: 280,
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: Size.pannelPadding
  },
  imageContainer: {
    width: imgSize + borderSize*2,
    height: imgSize + borderSize*2,
    borderRadius: '50%',
    backgroundColor: Color.bgAccent,
    marginBottom: 12
  },
  image: {
    width: imgSize,
    height: imgSize,
    left: borderSize,
    top: borderSize
  },
  subheading: {
    textAlign: 'center'
  },
  name: {
    color: Color.textIntroContactPrimary
  },
  careerTitle: {
    color: Color.textIntroContactPrimary,
    fontWeight: 300
  },
  aboutMe: {
    color: Color.textIntroContactSecondary,
    margin: 20,
    textAlign: 'center'
  },
  contactItems: {
    justifyContent: 'left'
  },
  contactContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 12
  },
  contactIcon: {
    color: Color.textIntroContactPrimary
  },
  contactText: {
    color: Color.textIntroContactSecondary,
    marginLeft: 12,
    textDecoration: 'none'
  }
};

export default styles