/*
 * Author: Anje Barnard
 */

import React from 'react'
import styles from './styles'
import Typography from 'material-ui/Typography'
import PropTypes from 'prop-types'

class OtherEntry extends React.Component {

  static propTypes = {
    title: PropTypes.string,
    entryItems: PropTypes.array.isRequired
  };

  render() {
    return (
      <div style={styles.entryContainer}>
        { this.props.title && <Typography type='body2' style={styles.title}>{this.props.title}</Typography> }
        {
          this.props.entryItems.map((entryItem) => (
            <p key={entryItem.accentText} style={styles.textContainer}>
              <span style={styles.accentText}>{`${entryItem.accentText}:`}</span>
              <span>  </span>
              <span style={styles.mainText}>{entryItem.mainText}</span>
            </p>
          ))
        }
      </div>
    )
  }
}

export default OtherEntry
