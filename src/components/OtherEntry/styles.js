/*
 * Author: Anje Barnard
 */

import Color from 'Color.js'
import Size from 'Size'

const styles = {
  entryContainer: {
    marginBottom: 10
  },
  title: {
    color: Color.textMainPrimary,
    marginBottom: Size.subheadingMargin
  },
  textContainer: {
    WebkitMarginBefore: 0,
    WebkitMarginAfter: 0
  },
  accentText: {
    color: Color.textAccent,
    fontSize: Size.fontSizeEntryBody
  },
  mainText: {
    color: Color.textMainSecondary,
    fontSize: Size.fontSizeEntryBody
  }
};

export default styles