/*
 * Author: Anje Barnard
 */

import Color from 'Color.js'
import Size from 'Size'

const styles = {
  headerContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  leftText: {
    color: Color.textMainPrimary,
    marginRight: 10
  },
  rightTextContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightText: {
    color: Color.textAccent,
    marginLeft: 10
  },
  divider: {
    width: 1,
    height: '0.75em',
    backgroundColor: Color.textMainSecondary
  },
  bullets: {
    WebkitMarginBefore: Size.subheadingMargin,
    WebkitMarginAfter: Size.subheadingMargin,
    WebkitPaddingStart: 18
  },
  bullet: {
    color: Color.textMainSecondary,
    fontSize: Size.fontSizeEntryBody
  },
  singleBullet: {
    color: Color.textMainSecondary,
    fontSize: Size.fontSizeEntryBody,
    marginTop: Size.subheadingMargin,
    marginBottom: Size.subheadingMargin
  }
};

export default styles