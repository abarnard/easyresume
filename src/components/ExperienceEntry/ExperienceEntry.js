/*
 * Author: Anje Barnard
 */

import React from 'react'
import styles from './styles'
import Typography from 'material-ui/Typography'
import PropTypes from 'prop-types'

class ExperienceEntry extends React.Component {

  static propTypes = {
    leftText: PropTypes.string.isRequired,
    rightText: PropTypes.string,
    bullets: PropTypes.array.isRequired
  };

  render() {
    return (
      <div>
        <div style={styles.headerContainer}>
          <Typography type='body2' style={styles.leftText}>{this.props.leftText}</Typography>
          {
            this.props.rightText && <Typography type='body2' style={styles.rightTextContainer}>
                                      <div style={styles.divider}/>
                                      <div style={styles.rightText}>{this.props.rightText}</div>
                                    </Typography>
          }
        </div>
        <ul style={styles.bullets}>
          {
            this.props.bullets.map((bullet, i) => (
              <li key={`bullet-${i}`} style={styles.bullet}>{bullet}</li>
            ))
          }
        </ul>
      </div>
    )
  }
}

export default ExperienceEntry