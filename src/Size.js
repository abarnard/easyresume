/*
 * Author: Anje Barnard
 */

const Size = {
  imgSmall: 48,
  imgMedium: 72,
  imgLarge: 144,
  imgXtraLarge: 192,
  pannelPadding: 16,
  headingMargin: 12,
  subheadingMargin: 4,
  fontSizeEntryBody: 12
};

export default Size