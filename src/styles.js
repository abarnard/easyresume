/*
 * Author: Anje Barnard
 */

const styles = {
  appContainer: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    overflow: 'auto'
  }
}

export default styles