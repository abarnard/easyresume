/*
 * Author: Anje Barnard
 */

const Color = {
  bgIntroContact: '#546e7a',
  bgMain: '#fafafa',
  bgAccent: '#26a69a',
  textMainPrimary: '#37474f',
  textMainSecondary: '#455a64',
  textIntroContactPrimary: '#e0e0e0',
  textIntroContactSecondary: '#eeeeee',
  textAccent: '#009688'
};

export default Color