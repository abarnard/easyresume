/*
 * Author: Anje Barnard
 */

import LeftPanel from 'components/LeftPanel'
import MainPanel from 'components/MainPanel'
import React from 'react'
import styles from 'styles'
import 'typeface-roboto'

class App extends React.Component {
  render() {
    return (
      <div style={styles.appContainer}>
        <LeftPanel name='Anje Barnard' jobTitle='Software Developer'/>
        <MainPanel name='Anje Barnard'/>
      </div>
    );
  }
}

export default App
