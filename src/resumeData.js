const resumeData = {
  name: 'Anje Barnard',
  careerTitle: 'Software Developer',
  intro: 'I’m a very loyal, hard working employee and I work well individually or as a team member. I enjoy working in the tech industry with people who want to learn more and build awesome things. Software development processes, technology stacks and languages evolve so quickly and I have found it is important to be flexible and keep on learning and improving.',
  phone: '+1 (902) 402 0709',
  email: 'anjebarnard@gmail.com',
  web: 'https://anjedroid.com',
  locationHome: 'Halifax, Nova Scotia, Canada',
  experience: [
    {
      title: 'Software Developer, Velo Industries Ltd.',
      date: 'Sept 2016 - Dec 2017',
      bullets: [
        'Web (Javascript, Firebase) and hybrid (Cordova) messaging app',
        'I was privileged to have worked with a very talented small team, where each developer worked on both frontend and backend to complete features.',
        'Participated in sprint planning, software design sessions, production health checks, regression testing after a sprint.'
      ]
    },
    {
      title: 'Senior Mobile Developer, 4iMobile',
      date: 'June 2015 - June 2016',
      bullets: [
        'Android development (new apps, features, maintenance)',
        'Involved with 5+ Android apps and 1 React.js app',
        'Team lead on various projects',
        'Collaborated with our UI/UX department to solve and improve our app planning before and during development',
        'Helped with technical analysis before development started',
        'Assisted/trained junior developers',
        'Helped interview prospective developers'
      ]
    },
    {
      title: 'Mobile Developer, 4iMobile',
      date: 'Dec 2012 - May 2015',
      bullets: [
        'Android development (new apps, features, maintenance)',
        'Involved with 10+ Android apps, 3 Blackberry apps and 1 iOS prototype app',
        'Integrated with APIs (JSON, XML, RESTful API)'
      ]
    },
    {
      title: 'Student Demi, Stellenbosch University',
      date: '2012',
      bullets: [
        'Assisted engineering students with Java basics (guidance and tutoring as needed). Graded assignments.'
      ]
    }
  ],
  education: [
    {
      title: 'B.Sc. (Hons) in Computer Science @ Stellenbosch University',
      date: '2012 - 2013',
      bullets: [
        'Coursework: Mobile Computing, Automata Theory, Databases (Enterprise Architecture), Computer Vision, Software Construction & Advanced Topics in Computer Science.'
      ]
    },
    {
      title: 'B.Sc. Mathematical Sciences @ Stellenbosch University',
      date: '2009 - 2011',
      bullets: [
        'Coursework: Operating Systems & Computer Architecture, Databases & Web Dev, Algorithms & Data structures, Concurrency, Program Design, Computer Networks, Scientific Computing, Operations Research, Socio-Informatics, First year Mathematics.'
      ]
    }
  ],
  other: [
    {
      sectionName: 'Projects',
      sectionIcon: 'StarBorder',
      entries: [
        {
          title: 'Most Recent Work',
          items: [
            {
              accentText: 'Web and hybrid (Cordova) apps',
              mainText: 'Velo - Messaging app with useful widgets to help everyday business messaging.'
            }
          ]
        },
        {
          title: 'Previous Work',
          items: [
            {
              accentText: 'Android',
              mainText: 'Worked on 15+ Android apps to name a few: Freelway Go (Swedish community delivery app), Varsity Sports (Wallet and ticketing app), Takealot (online shopping), Checkers (Specials and coupons app), Group Connect, Vaseline (In-store app)'
            },
            {
              accentText: 'Blackberry',
              mainText: 'Worked on Gumtree (South Africa\'s Kijiji) and Checkers'
            },
            {
              accentText: 'iOS',
              mainText: 'Touch to Learn: A prototype app to educate employees (new processes, onboarding etc.).'
            },
            {
              accentText: 'React',
              mainText: 'Finish and add features for a web app used to manage and track sensory devices that travel with shipping containers to monitor the goods.'
            }
          ]
        },
        {
          title: 'Personal Projects',
          items: [
            {
              accentText: 'EasyResume',
              mainText: 'React - Phase 1 was writing this resume in React in preparation for phase 2, which will allow others to export their own resume (pdf and/or web app).'
            },
            {
              accentText: 'VoiceThingy',
              mainText: 'Android - Small Android app to try out the newer Android Kotlin language. The app uses Android\'s speech recognition library to transform speech to text and then allow the user to share the text to other apps like Gmail, Twitter, Slack, etc.'
            },
            {
              accentText: 'PetHomie',
              mainText: 'Android - I started with this app to show my skills as well as assist pet owners to find loving homes for their pets. (project is at a pause at the moment, but would love to finish and release it in the near future)'
            }
          ]
        }
      ]
    },
    {
      sectionName: 'Languages & Technology',
      sectionIcon: 'AllInclusive',
      entries: [
        {
          items: [
            {
              accentText: 'Languages, concepts, tools',
              mainText: 'Java, Android, Javascript, React, Angular, HTML, css, JSON, XML, RESTful API, Firebase (Storage, Functions, Database)'
            },
            {
              accentText: 'IDEs',
              mainText: 'Android Studio, Sublime Text, Eclipse'
            },
            {
              accentText: 'Process',
              mainText: 'Agile, Trello, Asana, Jira, Github, Bitbucket'
            }
          ]
        }
      ]
    }
  ]
};

export default resumeData
